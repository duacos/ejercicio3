
package com.swing.components;

public class Components {
    private String nomVar;
    private String color;
    private String text;

    public Components(String nomVar, String color, String text) {
        this.nomVar = nomVar;
        this.color = color;
        this.text = text;
    }
    
    public void viewComponents() {
        System.out.println(""
                + "nomVar: " + this.nomVar + "\n"
                + "color: " + this.color + "\n"
                + "text: " + this.text
        );
    }
    
    public String getNomVar() {
        return nomVar;
    }

    public void setNomVar(String nomVar) {
        this.nomVar = nomVar;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }  
}