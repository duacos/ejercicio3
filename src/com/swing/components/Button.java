/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swing.components;

/**
 *
 * @author duacos
 */
public class Button extends Components {

    public Button(String nomVar, String color, String text) {
        super(nomVar, color, text);
    }
    
    public void actionButton() {
        System.out.println("Button action");
    }
    
    public void viewComponents() {
        super.viewComponents();
        System.out.println("Otros datos del button ...");
    }
    
    
}
