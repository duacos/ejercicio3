
package com.swing.components;

import com.swing.events.Listener;


public class Ejercicio1 {


    public static void main(String[] args) {
        
        Label label = new Label("label1", "#000000", "Texto del label1");
        Button button = new Button("Button1", "#454545", "Texto del button1");
        
        Listener labelListener = new Listener(label);
        Listener buttonListener = new Listener(button);
        
        labelListener.pulsoBotonIzquierdo();
        System.out.println("--------------------------");
        buttonListener.pulsoBotonIzquierdo();
        

    }
    
}
