
package com.swing.events;

import com.swing.components.Components;

public class Listener implements Clickeable {
    
    
    private Components components;

    public Listener(Components components) {
        this.components = components;
    }

    @Override
    public Components getComponents() {
        return this.components;
    }

    @Override
    public void setComponents(Components components) {
        this.components = components;
    }

    @Override
    public void pulsoBotonIzquierdo() {
        System.out.println("Se ha pulsado el click izquierdo");
        this.components.viewComponents();
    }

    @Override
    public void pulsoBotonDerecho() {
        System.out.println("Se ha pulsado el click derecho");
    }


    
}
