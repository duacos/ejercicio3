/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swing.events;

import com.swing.components.Components;

/**
 *
 * @author duacos
 */
public interface Clickeable {
    
    public abstract Components getComponents(); 
    public abstract void setComponents(Components components); 
    public abstract void pulsoBotonIzquierdo(); 
    public abstract void pulsoBotonDerecho(); 
    
}
